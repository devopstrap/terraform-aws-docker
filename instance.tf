provider "aws" {
    region = var.region
}

resource "aws_instance" "inst1" {
    ami = var.amiID
    instance_type = var.type
    # subnet_id = "subnet-62606418"
    associate_public_ip_address = true
    key_name = aws_key_pair.advocatediablo.key_name
    user_data = file("userdata.sh") 
    root_block_device {
        volume_size = "40"
        volume_type = "standard"
    }

    tags = {
        Name = "server1"
    }
    depends_on = [aws_key_pair.advocatediablo]
}

resource "aws_key_pair" "advocatediablo" {
  key_name   = "id_san3"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDXlV6YwyyiqJl+xjMQLhaeIIRS7jJwk/t4mm2NoFnauvv8sqSZLY0qNW9B9XJObIsa9bkJY6RXcYoQaujKXjJhWQN6cJf4KfPbHP7IeBtltaCW0DD/9dnULac82GuTeYzn0UWeJaL76qvbmXJBDdRj+3MaQ/o+AxJ7aQki75btMjn5c/ag8F0HxddL/3jlQ+fLwYLUDipLigU3MgdMaXMCqD73By3FhlP+a5Q7KZJPLNHj0DpoLETr55LCunxAHVWamAG8+wLqvI1JsUW/tofwYn37e7nHclrSDS4f6n8KMaBWH6M88N8SBMXMFuG9P/z4mobYX8/xrsflTOX8fUl0fYLs5nicGOD4uORLN1gGuOLQfND/AdzSF9hALSIn28bJrJrS7OaT3VItHfE3XiekNPUcEH46eVUmUJXrwrteoOvx6D6Q4+bqM2qUNKc5+TpkCVZS4Fs1ujO5Dt4YEYK5T9iQJQECnFZbXfVO+CqPsvLXzLccGLismx32lm2YlT1GAnWEM6e3hs1vYzPcobIBuJsuvWUfmSitA+X8m9t4rLQbdrwEFWi53pb3+iXJr3FV5ayePTFY3gROdUEf/dGVrca2yOFXfqus/D3locQnpE4zMWsXpagzFV328Vuqht5iq2TmAOjMDqdRQxPcHvOCn2IG73Z7NIP5scdNDvMICw== advocatediablo@mail.com"
}